//
//  AppDelegate.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 10/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "AppDelegate.h"
#import <DBChooser/DBChooser.h>
#import <DBChooser/DBChooserResult.h>



@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"Launch...%@",launchOptions);
    [Singleton sharedSingleton].baseUrl=@"http://dev.businessprodemo.com/MusicLynx/php/index.php/admin/";
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    NSLog(@"%@",sourceApplication);
    
   /* NSLog(@"Open URL:\t%@\n"
          "From source:\t%@\n"
          ,
          url, sourceApplication);*/
    if ([sourceApplication rangeOfString:@"Facebook"].location == NSNotFound)
    {
        if ([[DBChooser defaultChooser] handleOpenURL:url])
        {
            // This was a Chooser response and handleOpenURL automatically ran the
            // completion block
            return YES;
        }
        
        NSString *filepath = [url path];
        //...
        NSLog(@"Filepath...%@",filepath);
        
        // Move file to Our Tracks folder
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error = nil;
        NSString *srcPath = [[Singleton sharedSingleton] getTrackListDirectoryPath];
        srcPath = [srcPath stringByAppendingPathComponent:[[filepath componentsSeparatedByString:@"/"] lastObject]];
        @try {
            if([fileManager moveItemAtPath:filepath toPath:srcPath error:&error])
            {
                NSLog(@"Success");
                
                // Shoot notification to update the track list
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateAfterDownloadFromEmailNotification" object:self];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"error: %@", error);
        }
        
        /*NSError * error = nil;
         AVAudioPlayer *avPlayerObject =
         [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",filepath]] error:&error];
         if(avPlayerObject)
         {
         [avPlayerObject play];
         }*/
        
        
        
       
    }
    else
    {
       return [FBSession.activeSession handleOpenURL:url];
    }
     return YES;
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma facebook delegates
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    switch (state)
    {
        case FBSessionStateOpen:
        {
            if (FBSession.activeSession.isOpen)
            {
                [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
                 {
                     if (!error)
                     {
                         // NSLog(@"user_details: %@", user);
                         
                         NSLog(@"its allready open");
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"didLogInWithFacebookNotification" object:user];
                     }
                     else
                     {
                         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                      //   [SVProgressHUD dismiss];
                         NSLog(@"error=%@",error);
                     }
                 }];
            }
        }
            break;
        case FBSessionStateClosed:
            break;
        case FBSessionStateClosedLoginFailed:
            
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    if (error)
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        //[SVProgressHUD dismiss];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please allow Belinda  to access Facebook basic info to proceed further." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"stopSpinner" object:nil];
    }
}

- (void)openSession
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"email", @"public_profile", nil];
    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
     {
         [self sessionStateChanged:session state:state error:error];
     }];
}



@end
