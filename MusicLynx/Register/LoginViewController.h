//
//  LoginViewController.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "RegisterCell.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"

@interface LoginViewController : UIViewController{
    
    CGPoint pts;
    NSURLConnection     *_connection;
    NSMutableData *receivedData_;
    AppDelegate *app;
}

@property (strong,nonatomic) NSDictionary *mDirections;
@property (strong,nonatomic) NSString *mEmail;
@property (strong,nonatomic) NSString *mPassword;
@property (strong,nonatomic) NSString *mRegion;

@property (strong,nonatomic) NSString *mCalledService;

@property (strong, nonatomic) IBOutlet UIImageView *mainBg;
@property (strong, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (strong, nonatomic) IBOutlet UITableView *mTableView;
@property (strong, nonatomic) IBOutlet UIView *mDirectionView;
@property (strong, nonatomic) IBOutlet UIPickerView *mPickerView;
@property (strong, nonatomic) IBOutlet UIButton *mLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *mFacebookbtn;
- (IBAction)didTapfacebook:(id)sender;

- (IBAction)didTapDoneBtn:(id)sender;
- (IBAction)didTapForgotPassword:(id)sender;
- (IBAction)didTapLoginBtn:(id)sender;
- (IBAction)didTapRegisterBtn:(id)sender;

@end
