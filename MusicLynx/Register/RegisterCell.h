//
//  RegisterCell.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 10/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *registerTF;
@property (strong, nonatomic) IBOutlet UIImageView *formBoxImg;

@end
