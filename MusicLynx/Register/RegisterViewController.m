//
//  RegisterViewController.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 10/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0) {
        [_navBarImages setFrame:CGRectMake(0, 0, 320, 64)];
        [_navBarImages setImage:[UIImage imageNamed:@"nav_bar_IOS7.png"]];
        [_backButton setFrame:CGRectMake(6, 17, 50, 30)];
    }
    
    _mCalledService=@"region";
    
     NSString *post =@"";
     
     NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
     
     NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
     
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
     
     [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@manage_region/region_list",[Singleton sharedSingleton].baseUrl]]];
     [request setHTTPMethod:@"POST"];
     [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
     [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
     [request setHTTPBody:postData];
     
     _connection = [[NSURLConnection  alloc] initWithRequest:request delegate:self];
     
     if(_connection)
     receivedData_=[[NSMutableData  alloc] init];
     
     request = nil;
    
    
    if ([[Singleton sharedSingleton] is_iPhone5]) {
        //NSLog(@"iPhone5");
        [_mainBg setImage:[UIImage imageNamed:@"plainBg_i5.png"]];
    }
    else{
        //NSLog(@"iPhone4");
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //RegisterCell
    static NSString *cellID = @"RegisterCell";
    
    RegisterCell *cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell=[[RegisterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.registerTF.tag=indexPath.row+10;
    [cell.registerTF setReturnKeyType:UIReturnKeyDone];
    
    switch (indexPath.row) {
        case 0:
             cell.registerTF.placeholder=@"Producer name";
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;
        case 1:
            cell.registerTF.placeholder=@"Email";
            cell.registerTF.keyboardType=UIKeyboardTypeEmailAddress;
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;
        case 2:
            cell.registerTF.placeholder=@"Password";
            cell.registerTF.secureTextEntry=TRUE;
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;
        case 3:
            cell.registerTF.placeholder=@"Retype password";
            cell.registerTF.secureTextEntry=TRUE;
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;
        case 4:
            cell.registerTF.placeholder=@"Select Region";
            cell.formBoxImg.hidden=FALSE;
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;            
        default:
            break;
    }    
    return cell;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:_baseScrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 110;
    [_baseScrollView setContentOffset:pt animated:YES];
    
    if (textField.tag==14) {
        [textField setInputView:_mDirectionView];
    }    
}


// called when 'return' key pressed. return NO to ignore.

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
   
    [_baseScrollView setContentOffset:pts animated:YES];
    [textField resignFirstResponder];    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag==10) {
        _mName=textField.text;
    }
    else if (textField.tag==11){
        _mEmail=textField.text;
    }
    else if (textField.tag==12){
        _mPassword=textField.text;
    }
    else if (textField.tag==13){
        _mRetypePassword=textField.text;
    }
    else if (textField.tag==14) {
        textField.text=[[_mDirections valueForKey:@"region"] objectAtIndex:[_mPickerView selectedRowInComponent:0]];
        _mRegion=textField.text;
    }
    
    if ([_mName length]>0 && [_mEmail length]>0 && [_mPassword length]>0 &&[_mRetypePassword length] >0) {
        
        [_registerBtn setImage:[UIImage imageNamed:@"login2.png"] forState:UIControlStateNormal];
    }
    else{
        [_registerBtn setImage:[UIImage imageNamed:@"login1.png"] forState:UIControlStateNormal];
    }
}


//PickerViewDelegates

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return 4;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
   
   return [[_mDirections valueForKey:@"region"] objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    UITextField *tf = (UITextField *)[self.view viewWithTag:14];
    tf.text = [[_mDirections valueForKey:@"region"] objectAtIndex:[_mPickerView selectedRowInComponent:0]];
    //NSLog(@"%@",[_mDirections objectAtIndex:[_mPickerView selectedRowInComponent:0]]);
}

/////////////////////////////


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBaseScrollView:nil];
    [self setMDirectionView:nil];
    [self setMPickerView:nil];
    [self setMTableView:nil];
    [self setMainBg:nil];
    [self setRegisterBtn:nil];
    [super viewDidUnload];
}

- (IBAction)didTapDoneBtn:(id)sender {
        
    [_baseScrollView setContentOffset:pts animated:YES];
    [_mTableView endEditing:YES];    
}

- (IBAction)didTapBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapRegisterBtn:(id)sender {
    
    if ([_mName length]==0) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Please enter Producer name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_mEmail length]==0){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Please enter Email Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![[Singleton sharedSingleton] validateEmail:_mEmail]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Enter valid Email Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_mPassword length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Please enter Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_mRegion length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Please select Region" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }    
    else if (![_mPassword isEqualToString:_mRetypePassword]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Password and Retypepassword mismatch" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
    _mCalledService=@"Register";
    NSString *post =[NSString stringWithFormat:@"name=%@&email=%@&password=%@&region=%@",_mName,_mEmail,_mPassword,_mRegion];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@login/register_webservice",[Singleton sharedSingleton].baseUrl]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    _connection = [[NSURLConnection  alloc] initWithRequest:request delegate:self];
    
    if(_connection)
        receivedData_=[[NSMutableData  alloc] init];
    
    request = nil;
   }
    
}

#pragma mark-
#pragma mark  Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData_ setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData_ appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *retVal=[[NSString alloc]initWithData:receivedData_ encoding:NSUTF8StringEncoding];
    
    retVal=[retVal stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSLog(@"RetVal....%@",retVal);
    
    if ([_mCalledService isEqualToString:@"region"]) {
        _mDirections = [NSJSONSerialization JSONObjectWithData:receivedData_
                                                       options:kNilOptions
                                                         error:nil];
        NSLog(@"Result....%@",_mDirections);
    }
    else{
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:receivedData_
                                                               options:kNilOptions
                                                                 error:nil];
     NSLog(@"Result....%@",result);
    
    if ([_mCalledService isEqualToString:@"Register"]) {
        
     if ([[[result valueForKey:@"message"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"Failed"]) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"MusicLynx" message:@"Registration failed please try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
     }
    else if ([[[result valueForKey:@"message"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"Email Already Exist."]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"MusicLynx" message:@"Email Id already exist. Register with another one." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
     else if([[result valueForKey:@"message"] isEqualToString:@"Success"]){
         [self performSegueWithIdentifier:@"segueToNotificationViewController" sender:nil];
    }
   }
    else{
        
        
   }
  }
  receivedData_=nil;
}


@end
