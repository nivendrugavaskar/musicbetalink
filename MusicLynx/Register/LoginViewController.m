//
//  LoginViewController.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController{
    
    //BOOL loginSuccess;
    UIToolbar *toolBar;
    UIBarButtonItem *prev;
    UIBarButtonItem *next;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    prev = [[UIBarButtonItem alloc] initWithTitle:@"Prev" style:UIBarButtonItemStyleBordered target:self action:@selector(tapPrev)];
    
    next = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(tapNext)];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(tapDone)];
    
    [toolBar setAutoresizesSubviews:YES];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    [toolBar setTranslucent:YES];
    [toolBar setItems:[NSArray arrayWithObjects:prev,next,flex,done,nil]];
    
    if ([[Singleton sharedSingleton] is_iPhone5]) {
        NSLog(@"iPhone5");
        [_mainBg setImage:[UIImage imageNamed:@"plainBg_i5.png"]];
    }
    else{
       NSLog(@"iPhone4");
    }
    
    //NSLog(@"Length....%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Username"]);
    
    /*if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"Username"]length]>0){
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        UIViewController *mView = (UITabBarController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
        [self.navigationController pushViewController:mView animated:YES];
    }
    else{
    
    _mCalledService=@"region";
   }*/
    
    // Fill up login credentials
    _mEmail = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    _mPassword = [[NSUserDefaults standardUserDefaults] valueForKey:@"password"];
    [_mTableView reloadData];
    [self setLoginImage];
    
    if((_mEmail && _mEmail.length > 0) && (_mPassword && _mPassword.length > 0))
    {
        [self didTapLoginBtn:nil];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
}


-(void)tapPrev{
    
    [[self.view viewWithTag:10] becomeFirstResponder];
}

-(void)tapNext{
    
    [[self.view viewWithTag:11] becomeFirstResponder];
}

-(void)tapDone{
    
    [_mScrollView setContentOffset:pts animated:YES];
    [self.view endEditing:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //RegisterCell
    static NSString *cellID = @"RegisterCell";
    
    RegisterCell *cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell=[[RegisterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.registerTF.tag=indexPath.row+10;
    [cell.registerTF setReturnKeyType:UIReturnKeyDone];
    
    switch (indexPath.row) {
        case 0:
            cell.registerTF.placeholder=@"Email";
            if(_mEmail && _mEmail.length > 0)
            {
                cell.registerTF.text = _mEmail;
            }
            cell.registerTF.keyboardType=UIKeyboardTypeEmailAddress;
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;
        case 1:
            cell.registerTF.placeholder=@"Password";
            if(_mPassword && _mPassword.length > 0)
            {
                cell.registerTF.text = _mPassword;
            }
            cell.registerTF.secureTextEntry=TRUE;
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;
        case 2:
            cell.registerTF.placeholder=@"Select Region";
            cell.formBoxImg.hidden=FALSE;
            [cell.registerTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
            break;
            
        default:
            break;
    }
    
    return cell;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [textField setInputAccessoryView:toolBar];
    switch (textField.tag) {
        case 10:
            prev.enabled=FALSE;
            next.enabled=TRUE;
            break;
        case 11:
            prev.enabled=TRUE;
            next.enabled=FALSE;
            break;
        default:
            break;
    }
    
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:_mScrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 110;
    [_mScrollView setContentOffset:pt animated:YES];
    
    if (textField.tag==12) {
        [textField setInputView:_mDirectionView];
    }
}


// called when 'return' key pressed. return NO to ignore.

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [_mScrollView setContentOffset:pts animated:YES];
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag==10){
        _mEmail=textField.text;
    }
    else if (textField.tag==11){
        _mPassword=textField.text;
    }
    else if (textField.tag==12) {
         textField.text=[[_mDirections valueForKey:@"region"] objectAtIndex:[_mPickerView selectedRowInComponent:0]];
        _mRegion=textField.text;
    }
    
    [self saveLoginCredentials];
    
    [self setLoginImage];
}

- (void) setLoginImage
{
    if ([_mEmail length]>0 && [_mPassword length]>0) {
        [_mLoginBtn setImage:[UIImage imageNamed:@"login2.png"] forState:UIControlStateNormal];
    }
    else{
        [_mLoginBtn setImage:[UIImage imageNamed:@"login1.png"] forState:UIControlStateNormal];
    }
}


//PickerViewDelegates

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [_mDirections count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [[_mDirections valueForKey:@"region" ]objectAtIndex:row];    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    UITextField *tf = (UITextField *)[self.view viewWithTag:12];
    tf.text = [[_mDirections valueForKey:@"region" ] objectAtIndex:[_mPickerView selectedRowInComponent:0]];
    //NSLog(@"%@",[_mDirections objectAtIndex:[_mPickerView selectedRowInComponent:0]]);
}


#pragma mark-
#pragma mark  Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData_ setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData_ appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *retVal=[[NSString alloc]initWithData:receivedData_ encoding:NSUTF8StringEncoding];
    
    retVal=[retVal stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSLog(@"RetVal....%@",retVal);
    
    if ([_mCalledService isEqualToString:@"region"]) {
        _mDirections = [NSJSONSerialization JSONObjectWithData:receivedData_
                                                       options:kNilOptions
                                                         error:nil];        
        NSLog(@"Result....%@",_mDirections);
        [[Singleton sharedSingleton] setUserName:[_mDirections objectForKey:@"name"]];
    }
    
    if ([_mCalledService isEqualToString:@"login"]) {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:receivedData_
                                                       options:kNilOptions
                                                         error:nil];
        NSLog(@"Result....%@",result);
        [[Singleton sharedSingleton] setUserName:[result objectForKey:@"name"]];
        
        if ([[result valueForKey:@"message"] isEqualToString:@"Failed"]) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"MusicLynx" message:@"Invalid Username or Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if([[result valueForKey:@"message"] isEqualToString:@"Success"]){
            //loginSuccess=TRUE;
            //_mEmail=@"";
            //_mPassword=@"";
            //_mRegion=@"";
            NSLog(@"%@",[result valueForKey:@"name"]);
            //[[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[result valueForKey:@"name"]] forKey:@"Username"];
            //NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Username"]);
            [_mTableView reloadData];
            
            [self saveLoginCredentials];
            
            // Goto home page
            [self performSegueWithIdentifier:@"segueToLoginViewController" sender:nil];
        }
    }
    receivedData_=nil;
}


- (void) saveLoginCredentials
{
    // Save login credentials for future auto login
    [[NSUserDefaults standardUserDefaults] setValue:_mEmail forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setValue:_mPassword forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMScrollView:nil];
    [self setMTableView:nil];
    [self setMDirectionView:nil];
    [self setMPickerView:nil];
    [self setMainBg:nil];
    [self setMLoginBtn:nil];
    [super viewDidUnload];
}
- (IBAction)didTapfacebook:(id)sender
{
     [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
     [SVProgressHUD showWithStatus:@"Authenticating.."];
    
    app->logintype=@"fblogin";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginViaFacebook:) name:@"didLogInWithFacebookNotification" object:nil];
    [(AppDelegate *)[UIApplication sharedApplication].delegate openSession];
    
}

- (void)loginViaFacebook:(NSNotification *) notification
{
    
    NSDictionary *user = notification.object;
    NSLog(@"notificationdict: %@", user);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // NSDictionary *parameter=[NSDictionary dictionaryWithObjectsAndKeys:accountId,@"fb_id",email,@"email", nil];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
    //[self webServiceMethodForFaceBookLogin:user];
    //[self performSegueWithIdentifier:@"segueToHome" sender:nil];
    // app->userid=@"9";
    
    // Goto home page
    [self performSegueWithIdentifier:@"segueToLoginViewController" sender:nil];
    
}

- (IBAction)didTapDoneBtn:(id)sender {
    
    [_mScrollView setContentOffset:pts animated:YES];
    [_mTableView endEditing:YES];
    
}

- (IBAction)didTapForgotPassword:(id)sender {
    
    UIAlertView *mAlertView = [[UIAlertView alloc] initWithTitle:@"Forgot Password?" message:@"\n\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];    
    UITextField *mTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    mTextField.placeholder=@"Enter email id";
    [mTextField becomeFirstResponder];
    [mTextField setBackgroundColor:[UIColor whiteColor]];
    mTextField.textAlignment=NSTextAlignmentCenter;
    [mAlertView addSubview:mTextField];
    [mAlertView show];    
}

- (IBAction)didTapLoginBtn:(id)sender {
    
    if ([_mEmail length]==0) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Please enter Email Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];        
    }
    else if (![[Singleton sharedSingleton] validateEmail:_mEmail]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Enter valid Email Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_mPassword length]==0){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Please enter Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];        
    }
    else
    {
    
        app->logintype=@"normallogin";
        _mCalledService=@"login";
        NSString *post =[NSString stringWithFormat:@"email=%@&password=%@",_mEmail,_mPassword];
        //NSString *post =[NSString stringWithFormat:@"email=testing@gmail.com&password=12345"];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];

        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];

        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@login/login_webservice",[Singleton sharedSingleton].baseUrl]]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];

        _connection = [[NSURLConnection  alloc] initWithRequest:request delegate:self];

        if(_connection)
            receivedData_=[[NSMutableData  alloc] init];

        request = nil;
   }
}

- (IBAction)didTapRegisterBtn:(id)sender {
    
    [self performSegueWithIdentifier:@"segueToRegisterViewController" sender:_mDirections];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"Sender....%@",sender);
    if ([[segue identifier] isEqualToString: @"segueToRegisterViewController"]){
         RegisterViewController *dest = (RegisterViewController *)[segue destinationViewController];
        //the sender is what you pass into the previous method
         dest.mDirections=sender;
    }
}

@end
