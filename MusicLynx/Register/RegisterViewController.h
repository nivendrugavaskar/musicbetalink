//
//  RegisterViewController.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 10/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterCell.h"
#import "AppDelegate.h"

@interface RegisterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    
    CGPoint pts;
    NSURLConnection  *_connection;
    NSMutableData *receivedData_;
}

@property (strong,nonatomic) NSDictionary *mDirections;
@property (strong,nonatomic) NSString *mName;
@property (strong,nonatomic) NSString *mEmail;
@property (strong,nonatomic) NSString *mPassword;
@property (strong,nonatomic) NSString *mRetypePassword;
@property (strong,nonatomic) NSString *mRegion;
@property (strong,nonatomic) NSString *mCalledService;

@property (strong, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (strong, nonatomic) IBOutlet UIView *mDirectionView;
@property (strong, nonatomic) IBOutlet UIPickerView *mPickerView;
@property (strong, nonatomic) IBOutlet UITableView *mTableView;
@property (strong, nonatomic) IBOutlet UIImageView *mainBg;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UIImageView *navBarImages;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)didTapDoneBtn:(id)sender;
- (IBAction)didTapBackBtn:(id)sender;
- (IBAction)didTapRegisterBtn:(id)sender;

@end
