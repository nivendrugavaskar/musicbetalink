//
//  Singleton.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 24/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

@synthesize baseUrl;

+ (Singleton *)sharedSingleton
{
	static Singleton *sharedSingleton;
	
	@synchronized(self)
	{
		if (!sharedSingleton)
			sharedSingleton = [[Singleton alloc] init];
		
		return sharedSingleton;
	}
}


- (BOOL) connectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
	
    //Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
	
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
	
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return 0;
    }
	
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}


-(BOOL)is_iPhone5{
    
    CGSize screenSize=[[UIScreen mainScreen]bounds].size;
    
    if (screenSize.height>500.0) {
        return YES;
    }
    else{
       return NO;
    }
}

- (BOOL) validateEmail: (NSString *) checkString {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}



- (NSString *)getTrackListDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Tracks"];
    BOOL isDir;
    if(![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory isDirectory:&isDir])
    {
        NSError *error;
        if([[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error])
        {
            NSLog(@"Y");
        }
        else
        {
            NSLog(@"error: %@", error);
        }
    }
    
    return documentsDirectory;
}


@end
