//
//  AppDelegate.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 10/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//


/* 
 Login:
 http://dev.businessprodemo.com/MusicLynx/php/index.php/admin/login/login_webservice
 
 With value → email, password, region
 
 Register:
 http://dev.businessprodemo.com/MusicLynx/php/index.php/admin/login/register_webservice
 
 With value → name, email, password,region
 
 List all Regions:
 http://dev.businessprodemo.com/MusicLynx/php/index.php/admin/manage_region/region_list
 
 List of events:
 http://dev.businessprodemo.com/MusicLynx/php/index.php/admin/manage_event/event_list
 
 List of placement opportunities(By general/Featured):
 http://dev.businessprodemo.com/MusicLynx/php/index.php/admin/manage_placement_oppor/placement_oppor
 
 With value → po_type, index 
 */

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Singleton.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SVProgressHUD.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    @public
    NSString *logintype;
}

@property (strong, nonatomic) id audioPlayer;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) FBSession *session;
//- (BOOL)returnInternetConnectionStatus;
- (void)openSession;
@end
