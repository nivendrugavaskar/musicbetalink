//
//  PlacementListCell.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 26/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacementListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *mPlacementName;
@property (strong, nonatomic) IBOutlet UILabel *mPlacementDate;

@end
