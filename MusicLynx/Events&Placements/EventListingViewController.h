//
//  EventListingViewController.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventListCell.h"
#import "EventListingDetailViewController.h"
#import "EGORefreshTableHeaderView.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface EventListingViewController : UIViewController{
    
    NSURLConnection     *_connection;
    NSMutableData *receivedData_;
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
}

@property(strong,nonatomic) NSDictionary *eventsDetail;
@property(strong,nonatomic) NSArray *eventRegionKeys;

@property (weak, nonatomic) IBOutlet UIImageView *navBarImages;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UITableView *mEventTable;
@property (strong, nonatomic) IBOutlet UIImageView *mainBg;

- (IBAction)didTapBackBtn:(id)sender;

@end
