//
//  FeaturedViewController.h
//  BeatLinx
//
//  Created by Shuvra Karmakar on 20/11/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturedViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *mTableView;

@end
