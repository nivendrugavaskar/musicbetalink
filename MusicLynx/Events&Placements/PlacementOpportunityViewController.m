//
//  PlacementOpportunityViewController.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "PlacementOpportunityViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PlacementOpportunityViewController ()

@end

@implementation PlacementOpportunityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /*if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0) {
        [_navBarImages setFrame:CGRectMake(0, 0, 320, 64)];
        [_navBarImages setImage:[UIImage imageNamed:@"general_IOS7.png"]];
        [_backButton setFrame:CGRectMake(6, 17, 50, 30)];
     }*/
    
    
    _mPlacementTable.layer.cornerRadius=10.0f;
    [self loadData];
}

-(void)loadData{
    
    NSString *post =[NSString stringWithFormat:@"po_type=general&index=0"];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@manage_placement_oppor/placement_oppor",[Singleton sharedSingleton].baseUrl]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    _connection = [[NSURLConnection  alloc] initWithRequest:request delegate:self];
    
    if(_connection)
        receivedData_=[[NSMutableData  alloc] init];
    
    request = nil;    
}


#pragma mark-
#pragma mark  Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData_ setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData_ appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *retVal=[[NSString alloc]initWithData:receivedData_ encoding:NSUTF8StringEncoding];
    
    retVal=[retVal stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSLog(@"RetVal....%@",retVal);
    _placementDetail=[NSJSONSerialization JSONObjectWithData:receivedData_
                                                  options:kNilOptions
                                                    error:nil];
       
    receivedData_=nil;
        
    [_mPlacementTable reloadData];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_placementDetail count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"PlacementListCell";
    
    PlacementListCell *cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell=[[PlacementListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.mPlacementName.text=[[_placementDetail valueForKey:@"name"] objectAtIndex:indexPath.row];
    cell.mPlacementDate.text=[NSString stringWithFormat:@"%@ ,%@",[[_placementDetail valueForKey:@"po_date"] objectAtIndex:indexPath.row],[[_placementDetail valueForKey:@"po_time"] objectAtIndex:indexPath.row]];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"segueToPODetail" sender:[_placementDetail objectAtIndex:indexPath.row]];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString: @"segueToPODetail"]){
        PODetailViewController *mPO = (PODetailViewController *)[segue destinationViewController];
        //the sender is what you pass into the previous method
        mPO.poDetails=sender;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapBackBtn:(id)sender {
    
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload {
    [self setMainBg:nil];
    [self setMPlacementTable:nil];
    [super viewDidUnload];
}
@end
