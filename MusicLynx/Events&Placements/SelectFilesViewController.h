//
//  SelectFilesViewController.h
//  BeatLinx
//
//  Created by Gaurav Kumar on 23/10/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "SelectFileCell.h"

@interface SelectFilesViewController : UIViewController{
    
    MFMailComposeViewController *mailComposer;
    AVAudioPlayer *player;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mSpinner;
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UIImageView *navBarImages;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *mDeleteButton;

- (IBAction)didTapBackBtn:(id)sender;
- (IBAction)didTapCheckBtn:(id)sender;
- (IBAction)didTapEmailBtn:(id)sender;
- (IBAction)didTapDropBoxBtn:(id)sender;
- (IBAction)didTapDeleteButton:(id)sender;


@end
