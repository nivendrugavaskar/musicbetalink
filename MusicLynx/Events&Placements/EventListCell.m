//
//  EventListCell.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 25/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "EventListCell.h"

@implementation EventListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
