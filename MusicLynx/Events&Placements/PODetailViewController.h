//
//  PODetailViewController.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AppDelegate.h"


@interface PODetailViewController : UIViewController{
    
    MFMailComposeViewController *mailComposer;
}

@property(strong, nonatomic) NSArray *poDetails;

@property (strong, nonatomic) IBOutlet UILabel *poName;
@property (strong, nonatomic) IBOutlet UITextView *poInfo;
@property (strong, nonatomic) IBOutlet UIImageView *poIcon;
@property (weak, nonatomic) IBOutlet UIImageView *navBarImages;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

-(void)sendEmail:(NSString*)link;
- (IBAction)didTapBackBtn:(id)sender;
- (IBAction)didTapAddMusic:(id)sender;
@end
