//
//  FeaturedViewController.m
//  BeatLinx
//
//  Created by Shuvra Karmakar on 20/11/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "FeaturedViewController.h"

@interface FeaturedViewController ()

@end

@implementation FeaturedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)didTapBackButton:(id)sender
{
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

@end
