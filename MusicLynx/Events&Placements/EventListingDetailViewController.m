//
//  EventListingDetailViewController.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "EventListingDetailViewController.h"

@interface EventListingDetailViewController ()

@end

@implementation EventListingDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    /*if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0) {
        [_navBarImages setFrame:CGRectMake(0, 0, 320, 64)];
        [_navBarImages setImage:[UIImage imageNamed:@"event_IOS7.png"]];
        [_backButton setFrame:CGRectMake(6, 17, 50, 30)];
    }*/
    
    NSLog(@"EventDetail....%@",_eventDetail);
    
    _eventName.text=[_eventDetail valueForKey:@"name"];
    _eventInfo.text=[NSString stringWithFormat:@"%@\n%@, %@\n%@",[_eventDetail valueForKey:@"place"],[_eventDetail valueForKey:@"event_date"],[_eventDetail valueForKey:@"event_time"],[_eventDetail valueForKey:@"details"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapAddToCalender:(id)sender {
    
    //Create the Event Store
    EKEventStore *eventStore = [[EKEventStore alloc]init];
    
    //Check if iOS6 or later is installed on user's device *******************
    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        
        //Request the access to the Calendar
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted,NSError* error){
            
            //Access not granted-------------
            if(!granted){
                NSString *message = @"Hey! I Can't access your Calendar... check your privacy settings to let me in!";
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Warning"
                                                                   message:message
                                                                  delegate:self
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil,nil];
                //Show an alert message!
                //UIKit needs every change to be done in the main queue
                dispatch_async(dispatch_get_main_queue(), ^{
                    [alertView show];
                }
                               );
                
                //Access granted------------------
            }else{
                
                                             
                //Event created
                if([self createEvent:eventStore]){
                   
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Event added to Calendar" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [alert show];
                    });
                    
                    //Error occured
                }else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Event not added, something goes wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [alert show];
                    });
                }                
              }
        }];
    }
    
    //Device prior to iOS 6.0  *********************************************
    else{
        [self createEvent:eventStore];
    }
}

//Event creation
-(BOOL)createEvent:(EKEventStore*)eventStore{
    
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    event.title = [_eventDetail valueForKey:@"name"];
        
    NSString *dateString = [NSString stringWithFormat:@"%@ %@",[_eventDetail valueForKey:@"event_date"],[_eventDetail valueForKey:@"event_time"]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm"];
    NSDate *dateFromString = [[NSDate alloc] init];
    // voila!
    dateFromString = [dateFormatter dateFromString:dateString];
    
    event.startDate=dateFromString;
    event.endDate=dateFromString;
    event.calendar = [eventStore defaultCalendarForNewEvents];
    
    NSError *error;
    [eventStore saveEvent:event span:EKSpanThisEvent error:&error];
    
    if (error) {
        NSLog(@"Event Store Error: %@",[error localizedDescription]);
        return NO;
    }else{
        return YES;
    }
}

- (void)viewDidUnload {
    [self setEventName:nil];
    [self setEventDetail:nil];
    [self setEventDetail:nil];
    [self setEventInfo:nil];
    [self setEventImage:nil];
    [super viewDidUnload];
}
@end
