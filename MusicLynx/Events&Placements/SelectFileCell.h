//
//  SelectFileCell.h
//  BeatLinx
//
//  Created by Gaurav Kumar on 23/10/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectFileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *fileName;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBtn;

@end
