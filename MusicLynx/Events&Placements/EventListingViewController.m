//
//  EventListingViewController.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "EventListingViewController.h"


@interface EventListingViewController ()

@end

@implementation EventListingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    /*if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0) {
        [_navBarImages setFrame:CGRectMake(0, 0, 320, 64)];
        [_navBarImages setImage:[UIImage imageNamed:@"event_IOS7.png"]];
        [_backButton setFrame:CGRectMake(6, 17, 50, 30)];
     }*/
    
    
    _mEventTable.layer.cornerRadius=10.0f;
    [self loadData];
    
    //refresh
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.mEventTable.bounds.size.height, self.view.frame.size.width, self.mEventTable.bounds.size.height)];
		view.delegate = (id)self;
		[self.mEventTable addSubview:view];
		_refreshHeaderView = view;
	}	
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];    
}


-(void)loadData{
    
    NSString *post =@"";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@manage_event/event_list",[Singleton sharedSingleton].baseUrl]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    _connection = [[NSURLConnection  alloc] initWithRequest:request delegate:self];
    
    if(_connection)
        receivedData_=[[NSMutableData  alloc] init];
    
    request = nil;    
}



#pragma mark-
#pragma mark  Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData_ setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData_ appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *retVal=[[NSString alloc]initWithData:receivedData_ encoding:NSUTF8StringEncoding];
    
    retVal=[retVal stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSLog(@"RetVal....%@",retVal);
    _eventsDetail=[NSJSONSerialization JSONObjectWithData:receivedData_
                                                           options:kNilOptions
                                                            error:nil];
    //NSLog(@"Event Details....%@",[[_eventsDetail objectForKey:@"region" ] allKeys]);
    
    receivedData_=nil;
    
    _eventRegionKeys=[[_eventsDetail objectForKey:@"region"] allKeys];
    
    NSLog(@"Event1.....%@",_eventsDetail);
    [_mEventTable reloadData];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [_eventRegionKeys count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[[_eventsDetail objectForKey:@"region"]objectForKey:[_eventRegionKeys objectAtIndex:section]] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    return [_eventRegionKeys objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"EventListCell";
       
    EventListCell *cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell=[[EventListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.mEventName.text=[[[[_eventsDetail objectForKey:@"region"]objectForKey:[_eventRegionKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.mEventDate.text=[NSString stringWithFormat:@"%@ , %@",[[[[_eventsDetail objectForKey:@"region"]objectForKey:[_eventRegionKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] valueForKey:@"event_date"],[[[[_eventsDetail objectForKey:@"region"]objectForKey:[_eventRegionKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] valueForKey:@"event_time"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    [self performSegueWithIdentifier:@"segueToEventListingDetail" sender:[[[_eventsDetail objectForKey:@"region"]objectForKey:[_eventRegionKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   if ([[segue identifier] isEqualToString: @"segueToEventListingDetail"]){
        EventListingDetailViewController *mEvent = (EventListingDetailViewController *)[segue destinationViewController];
        //the sender is what you pass into the previous method
        mEvent.eventDetail=sender;
    }
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
    
    receivedData_ = nil;
    receivedData_ = [NSMutableData data];
    [self loadData];	
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.mEventTable];	
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed	
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapBackBtn:(id)sender {
    
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload {
    [self setMainBg:nil];
    [self setMEventTable:nil];
    [super viewDidUnload];
}
@end
