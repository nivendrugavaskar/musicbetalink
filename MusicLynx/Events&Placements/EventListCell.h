//
//  EventListCell.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 25/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *mEventImg;
@property (strong, nonatomic) IBOutlet UILabel *mEventName;
@property (strong, nonatomic) IBOutlet UILabel *mEventDate;

@end
