//
//  ViewController.m
//  STTwitterDemoiOSSafari
//
//  Created by Nicolas Seriot on 10/1/13.
//  Copyright (c) 2013 Nicolas Seriot. All rights reserved.
//

#import "TweetViewController.h"
#import "STTwitter.h"
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>

typedef void (^accountChooserBlock_t)(ACAccount *account, NSString *errorMessage); // don't bother with NSError for that

@interface TweetViewController ()
{
    NSString *myuserid;
    BOOL authenticatestatus;
}
@property (nonatomic, strong) STTwitterAPI *twitter;
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) ACAccount *useraccount;
@property (nonatomic, strong) NSArray *iOSAccounts;
@property (nonatomic, strong) accountChooserBlock_t accountChooserBlock;
@property(nonatomic, retain) NSMutableString *paramString;

@end

// https://dev.twitter.com/docs/auth/implementing-sign-twitter

@implementation TweetViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    myuserid=[[NSString alloc] init];
    
    self.accountStore = [[ACAccountStore alloc] init];
    
#pragma warning Replace these demo tokens with yours https://dev.twitter.com/apps
    
    _consumerKeyTextField.text = @"mBP9wuHQsW7OXRh34qU7xBjYa";
    _consumerSecretTextField.text = @"V1Kgy570ldcOSR5z9gZDz7cKqWCpDho3UeZEvlwMXnnT0Buhb3";
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    authenticatestatus=FALSE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginWithiOSAction:(id)sender {
    
    _loginStatusLabel.text = @"Trying to login with iOS...";

    __weak typeof(self) weakSelf = self;
    
    self.accountChooserBlock = ^(ACAccount *account, NSString *errorMessage) {
        
        NSString *status = nil;
        if(account) {
            status = [NSString stringWithFormat:@"Did select %@", account.username];
            
            [weakSelf loginWithiOSAccount:account];
        } else {
            status = errorMessage;
        }
        
        weakSelf.loginStatusLabel.text = status;
    };
    
    [self chooseAccount];
}

- (void)loginWithiOSAccount:(ACAccount *)account {
    
    self.twitter = [STTwitterAPI twitterAPIOSWithAccount:account];
    
    [_twitter verifyCredentialsWithUserSuccessBlock:^(NSString *username, NSString *userID)
     {
        
        _loginStatusLabel.text = [NSString stringWithFormat:@"@%@ (%@)", username, userID];
        // code
        myuserid=userID;
        _useraccount=[[ACAccount alloc] init];
        _useraccount=account;
         
         // new code
         authenticatestatus=TRUE;
        
    } errorBlock:^(NSError *error) {
        _loginStatusLabel.text = [error localizedDescription];
    }];
    
}


- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier {
    
    // in case the user has just authenticated through WebViewVC
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
    
    [_twitter postAccessTokenRequestWithPIN:verifier successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName) {
        NSLog(@"-- screenName: %@", screenName);
        
        _loginStatusLabel.text = [NSString stringWithFormat:@"%@ (%@)", screenName, userID];
        
        /*
         At this point, the user can use the API and you can read his access tokens with:
         
         _twitter.oauthAccessToken;
         _twitter.oauthAccessTokenSecret;
         
         You can store these tokens (in user default, or in keychain) so that the user doesn't need to authenticate again on next launches.
         
         Next time, just instanciate STTwitter with the class method:
         
         +[STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerSecret:oauthToken:oauthTokenSecret:]
         
         Don't forget to call the -[STTwitter verifyCredentialsWithSuccessBlock:errorBlock:] after that.
         */
        
    } errorBlock:^(NSError *error) {
        
        _loginStatusLabel.text = [error localizedDescription];
        NSLog(@"-- %@", [error localizedDescription]);
    }];
}

- (IBAction)didTapdismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)getTimelineAction:(id)sender
{
    if(!authenticatestatus)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"! Required" message:@"Please Authinticate your twitter account" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    self.getTimelineStatusLabel.text = @"";
    
//    [_twitter getFollowersListForUserID:myuserid orScreenName:nil count:@"20" cursor:@"test" skipStatus:nil includeUserEntities:nil successBlock:^(NSArray *users, NSString *previousCursor, NSString *nextCursor)
//    {
//        NSLog(@"%@",users);
//    }
//errorBlock:^(NSError *error)
//    {
//        NSLog(@"%@",[error localizedDescription]);
//    }];

    
//    [_twitter getHomeTimelineSinceID:nil
//                               count:20
//                        successBlock:^(NSArray *statuses) {
//                            
//                            NSLog(@"-- statuses: %@", statuses);
//                            
//                            self.getTimelineStatusLabel.text = [NSString stringWithFormat:@"%lu statuses", (unsigned long)[statuses count]];
//                            
//                            self.statuses = statuses;
//                            
//                            [self.tableView reloadData];
//                            
//                        } errorBlock:^(NSError *error) {
//                            self.getTimelineStatusLabel.text = [error localizedDescription];
//                        }];
    
    
    // new code
    [SVProgressHUD showWithStatus:@"Loading...."];
    
    [self getTwitterFriendsForAccount:_useraccount];

}

-(void)getTwitterFriendsForAccount:(ACAccount*)account
{
    // In this case I am creating a dictionary for the account
    // Add the account screen name
    NSMutableDictionary *accountDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:account.username, @"screen_name", nil];
    // Add the user id (I needed it in my case, but it's not necessary for doing the requests)
    [accountDictionary setObject:[[[account dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]] objectForKey:@"properties"] objectForKey:@"user_id"] forKey:@"user_id"];
    // Setup the URL, as you can see it's just Twitter's own API url scheme. In this case we want to receive it in JSON
   // NSURL *followingURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/followers/list.json"];
     NSURL *followingURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/friends/ids.json"];
    
  //  https://api.twitter.com/1.1/favorites/list.json
    // Pass in the parameters (basically '.ids.json?screen_name=[screen_name]')
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:account.username, @"screen_name", nil];
    // Setup the request
   /* TWRequest *twitterRequest = [[TWRequest alloc] initWithURL:followingURL
                                                    parameters:parameters
                                                 requestMethod:TWRequestMethodGET];*/
    
    
    SLRequest *aRequest  = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                              requestMethod:TWRequestMethodGET
                                                        URL:followingURL
                                                 parameters:parameters];
    // This is important! Set the account for the request so we can do an authenticated request. Without this you cannot get the followers for private accounts and Twitter may also return an error if you're doing too many requests
    [aRequest setAccount:account];
    // Perform the request for Twitter friends
    [aRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error) {
            // deal with any errors - keep in mind, though you may receive a valid response that contains an error, so you may want to look at the response and ensure no 'error:' key is present in the dictionary
        }
        NSError *jsonError = nil;
        // Convert the response into a dictionary
       // NSDictionary *twitterFriends = [NSJSONSerialization JSONObjectWithData:responseData options:options:NSJSONWritingPrettyPrinted error:&jsonError];
        
         NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@", responseStr);
         NSDictionary *tw=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&jsonError];
        
         NSArray *IDlist = [tw objectForKey:@"ids"];
        
        // Grab the Ids that Twitter returned and add them to the dictionary we created earlier
       // [accountDictionary setObject:[tw objectForKey:@"ids"] forKey:@"friends_ids"];
       // NSLog(@"%@", accountDictionary);
        int count = IDlist.count;
        _paramString=[[NSMutableString alloc] init];
        for (int i=0; i<count; i++ )
        {
            [_paramString appendFormat:@"%@",[IDlist objectAtIndex:i]];
            
            if (i <count-1)
            {
                NSString *delimeter = @",";
                [_paramString appendString:delimeter];
            }
            
        }
        
        NSLog(@"The mutable string is %@", _paramString);
        [self getFollowerNameFromID:_paramString];
    }];
}
// new code
-(void) getFollowerNameFromID:(NSString *)ID
{
    
    
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/friendships/lookup.json"];
    NSDictionary *p = [NSDictionary dictionaryWithObjectsAndKeys:ID, @"user_id",nil];
    NSLog(@"make a request for ID %@",p);
    
//    TWRequest *twitterRequest = [[TWRequest alloc] initWithURL:url
//                                                    parameters:p
//                                                 requestMethod:TWRequestMethodGET];
    SLRequest *twitterRequest  = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                              requestMethod:TWRequestMethodGET
                                                        URL:url
                                                 parameters:p];
    [twitterRequest setAccount:_useraccount];
    
    
    [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error) {
            
        }
        NSError *jsonError = nil;
        
        
        //NSDictionary *friendsdata = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONWritingPrettyPrinted error:&jsonError];
        NSDictionary *friendsdata=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&jsonError];
        //  NSLog(@"friendsdata value is %@", friendsdata);
        
        
        _statuses = [[NSArray alloc]init];
        _statuses = [friendsdata valueForKey:@"name"];
        NSLog(@"resultNameList value is %@", _statuses);
        [self.tableView reloadData];
        
        
        
    }];
    
}
#pragma end of loading

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        //for example [activityIndicator stopAnimating];
        [SVProgressHUD dismiss];
    }
}


- (void)chooseAccount {
    
    ACAccountType *accountType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    ACAccountStoreRequestAccessCompletionHandler accountStoreRequestCompletionHandler = ^(BOOL granted, NSError *error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            if(granted == NO) {
                _accountChooserBlock(nil, @"Acccess not granted.");
                return;
            }
            
            self.iOSAccounts = [_accountStore accountsWithAccountType:accountType];
            
            if([_iOSAccounts count] == 1) {
                ACAccount *account = [_iOSAccounts lastObject];
                _accountChooserBlock(account, nil);
            } else {
                UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Select an account:"
                                                                delegate:self
                                                       cancelButtonTitle:@"Cancel"
                                                  destructiveButtonTitle:nil otherButtonTitles:nil];
                for(ACAccount *account in _iOSAccounts) {
                    [as addButtonWithTitle:[NSString stringWithFormat:@"@%@", account.username]];
                }
                [as showInView:self.view.window];
            }
        }];
    };
    
#if TARGET_OS_IPHONE &&  (__IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0)
    if (floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_6_0) {
        [self.accountStore requestAccessToAccountsWithType:accountType
                                     withCompletionHandler:accountStoreRequestCompletionHandler];
    } else {
        [self.accountStore requestAccessToAccountsWithType:accountType
                                                   options:NULL
                                                completion:accountStoreRequestCompletionHandler];
    }
#else
    [self.accountStore requestAccessToAccountsWithType:accountType
                                               options:NULL
                                            completion:accountStoreRequestCompletionHandler];
#endif

}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.statuses count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"STTwitterTVCellIdentifier"];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"STTwitterTVCellIdentifier"];
    }
    
   
    NSString *text = [self.statuses objectAtIndex:indexPath.row];
    cell.textLabel.text = text;
    return cell;
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == [actionSheet cancelButtonIndex]) {
        _accountChooserBlock(nil, @"Account selection was cancelled.");
        return;
    }
    
    NSUInteger accountIndex = buttonIndex - 1;
    ACAccount *account = [_iOSAccounts objectAtIndex:accountIndex];
    
    _accountChooserBlock(account, nil);
}


@end
