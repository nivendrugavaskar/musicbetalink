//
//  PlacementOpportunityViewController.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlacementListCell.h"
#import "PODetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface PlacementOpportunityViewController : UIViewController{
    
    NSURLConnection *_connection;
    NSMutableData *receivedData_;
}

@property (strong, nonatomic) NSArray *placementDetail;
@property (strong, nonatomic) IBOutlet UIScrollView *mainBg;
@property (weak, nonatomic) IBOutlet UIImageView *navBarImages;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UITableView *mPlacementTable;

- (IBAction)didTapBackBtn:(id)sender;

@end
