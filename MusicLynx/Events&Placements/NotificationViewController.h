//
//  NotificationViewController.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 12/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface NotificationViewController : UIViewController
{
    AppDelegate *app;
}


@property (strong, nonatomic) IBOutlet UIImageView *mainBg;
@property (weak, nonatomic) IBOutlet UIImageView *navBarImages;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)didTapBackBtn:(id)sender;
- (IBAction)didTapRegularBtn:(id)sender;
- (IBAction)didTapEventsBtn:(id)sender;
- (IBAction)didTapFeaturedBtn:(id)sender;
- (IBAction)didTapMusicBtn:(id)sender;
- (IBAction)didTaptweet:(id)sender;


@end
