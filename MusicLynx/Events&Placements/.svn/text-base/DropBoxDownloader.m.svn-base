//
//  DropBoxDownloader.m
//  BeatLinx
//
//  Created by Shuvra Karmakar on 28/11/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "DropBoxDownloader.h"
#import <DBChooser/DBChooser.h>
#import <DBChooser/DBChooserResult.h>
#import "Singleton.h"

@implementation DropBoxDownloader
{
    DBChooserResult *_result;
    NSMutableData *receivedData_;
    NSString *currentURL;
    NSURLConnection *_connection;
    long expectedBytes;
    
    UIProgressView *progress;
    UIView *progressContainerView;
}

-(id)init
{
    if(!self)
    {
        self = [super init];
    }
    return self;
}

- (void) downloadFileInViewInView:(UIView *)view
{
    // check if dropbox is installed in the device
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"dbapi-1://"]]) {
        NSLog(@"Dropbox is not installed.");
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://itunes.apple.com/in/app/dropbox/id327630330"]];
        return;
    }
    
    progress = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, 200, 0)];
    progressContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, 100)];
    progressContainerView.layer.cornerRadius = 8.0f;
    progressContainerView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7];
    [progressContainerView addSubview:progress];
    progress.center = CGPointMake(progressContainerView.center.x, progressContainerView.center.y + 20);
    [view addSubview:progressContainerView];
    progressContainerView.center = view.center;
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    act.frame=CGRectMake(10, 10, 250, 250);
    [act setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    act.hidesWhenStopped = NO;
    act.center = CGPointMake(progress.center.x, progress.center.y - 25);
    [act startAnimating];
    [progressContainerView addSubview:act];
    progressContainerView.hidden = YES;
    
    DBChooserLinkType linkType =DBChooserLinkTypeDirect;
    [[DBChooser defaultChooser] openChooserForLinkType:linkType fromViewController:(id)self
                                            completion:^(NSArray *results)
     {
         if ([results count]) {
             _result = results[0];
         } else {
             _result = nil;
             [[[UIAlertView alloc] initWithTitle:@"CANCELLED" message:@"User cancelled!"
                                        delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
              show];
         }
         NSLog(@"File: %@", _result);
         //NSLog(@"FileName...%@ Size....%.2f",[_result name],(float)[_result size]/1024/1024);

         NSURL *url = [NSURL URLWithString:[[_result link] absoluteString]];
         currentURL = [[_result link] absoluteString];
         NSURLRequest *theRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
         receivedData_ = [[NSMutableData alloc] initWithLength:0];
         _connection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:YES];
     }];
}

#pragma mark  Connection Delegate Methods

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    progressContainerView.hidden = NO;
    [receivedData_ setLength:0];
    expectedBytes = [response expectedContentLength];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData_ appendData:data];
    float progressive = (float)[receivedData_ length] / (float)expectedBytes;
    [progress setProgress:progressive];
    NSLog(@"data count: %f/%f", progressive, (float)expectedBytes);
    
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}

- (NSCachedURLResponse *) connection:(NSURLConnection *)connection willCacheResponse:    (NSCachedURLResponse *)cachedResponse {
    return nil;
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *name = _result.name;
    NSString *folder = [[Singleton sharedSingleton] getTrackListDirectoryPath];
    NSString *filePath = [folder stringByAppendingPathComponent:name];
    BOOL isDir;
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDir])
    {
        BOOL status = NO;
        @try {
            if([receivedData_ writeToFile:filePath atomically:YES])
            {
                status = YES;
                // file saved alert
                UIAlertView *mAlert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"File has been saved to Music" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [mAlert show];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"exception: %@", exception);
            
            // show alert that file saving failed
            UIAlertView *mAlert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Failed to download file. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [mAlert show];
        }
        @finally {
            
            // tell the reciever that the file has been downloadded along with the finish status & path
            [self.delegate fileDownloadedWithStatus:status path:filePath];
        }
    }
    else
    {
        // File already exists
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BeatLinx" message:@"File already exists" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    progress.hidden = YES;
    progressContainerView.hidden = YES;
}

#pragma mark-


@end
