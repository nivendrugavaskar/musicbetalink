//
//  PODetailViewController.m
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import "PODetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DropBoxDownloader.h"

@interface PODetailViewController ()

@end

@implementation PODetailViewController{
    
    //DBChooserResult *_result; // result received from last CHooser call
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    /*if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0) {
        [_navBarImages setFrame:CGRectMake(0, 0, 320, 64)];
        [_navBarImages setImage:[UIImage imageNamed:@"general_IOS7.png"]];
        [_backButton setFrame:CGRectMake(6, 17, 50, 30)];
    }*/
    
    NSLog(@"getTrackListDirectoryPath: %@", [[Singleton sharedSingleton] getTrackListDirectoryPath]);
    
    _poName.text=[_poDetails valueForKey:@"name"];
    _poInfo.text=[NSString stringWithFormat:@"%@\n%@, %@\n%@",[_poDetails valueForKey:@"place"],[_poDetails valueForKey:@"po_date"],[_poDetails valueForKey:@"po_time"],[_poDetails valueForKey:@"details"]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setPoName:nil];
    [self setPoInfo:nil];
    [self setPoIcon:nil];
    [super viewDidUnload];
}

- (IBAction)didTapBackBtn:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapAddMusic:(id)sender{
    
    UIAlertView *mAlert=[[UIAlertView alloc]initWithTitle:@"BeatLinx" message:@"Select Music Files" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"DropBox",@"Saved Files", nil];
    [mAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSLog(@"Button Index...%d",buttonIndex);

    
    if (buttonIndex==1) {
        //DropBox
        /*
        DBChooserLinkType linkType =DBChooserLinkTypeDirect;
        [[DBChooser defaultChooser] openChooserForLinkType:linkType fromViewController:self
                                                completion:^(NSArray *results)
         {
             if ([results count]) {
                 _result = results[0];
             } else {
                 _result = nil;
                 [[[UIAlertView alloc] initWithTitle:@"CANCELLED" message:@"User cancelled!"
                                            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
                  show];
             }
             NSLog(@"Result...%@",[[_result link] absoluteString]);
             NSLog(@"FileName...%@ Size....%.2f",[_result name],(float)[_result size]/1024/1024);
             [self downloadFile:[[_result link] absoluteString]];
             //[self sendEmail:[[_result link] absoluteString]];
             
             
             
             
         }];*/
        
        DropBoxDownloader *dropBoxDownloader = [[DropBoxDownloader alloc] init];
        [dropBoxDownloader downloadFileInViewInView:self.view];
    }
    else if (buttonIndex==2){
        
       //Saved files
        [self performSegueWithIdentifier:@"segueSelectMusic" sender:nil];
    }
}


-(void)sendEmail:(NSString*)link{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]){
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = (id)self;
        
        NSArray *receiptArray = [[NSArray alloc] initWithObjects:[NSString stringWithFormat:@"%@",[_poDetails valueForKey:@"email"]], nil];
        [mailComposer setToRecipients:receiptArray];
        [mailComposer setSubject:[NSString stringWithFormat:@"Beats from %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];
        [mailComposer setMessageBody:[NSString stringWithFormat:@"\n\n\n\n\n\n\n\n\n\n\nConfidentiality Agreement:Please acknowledge that all materials downloaded through this E-mail are copywritten and solely owned by %@. All rights are reserved. By downloading this material you agree to not use in any way and (or) disclose this material to any third parties without the written consent of %@.",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]] isHTML:NO];
        [self presentViewController:mailComposer animated:YES completion:nil];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MoviePlanet" message:@"Mail is not configured in your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0)
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
