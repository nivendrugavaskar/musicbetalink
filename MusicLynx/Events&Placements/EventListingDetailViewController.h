//
//  EventListingDetailViewController.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 13/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKitUI/EventKitUI.h>
#import "RegisterCell.h"

@interface EventListingDetailViewController : UIViewController

@property(nonatomic,strong) NSDictionary *eventDetail;

@property (strong, nonatomic) IBOutlet UILabel *eventName;
@property (strong, nonatomic) IBOutlet UITextView *eventInfo;
@property (strong, nonatomic) IBOutlet UIImageView *eventImage;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *navBarImages;

- (IBAction)didTapBackBtn:(id)sender;
- (IBAction)didTapAddToCalender:(id)sender;

@end

