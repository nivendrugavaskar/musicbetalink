//
//  Singleton.h
//  MusicLynx
//
//  Created by Gaurav Kumar on 24/06/13.
//  Copyright (c) 2013 Gaurav Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#include <netinet/in.h>
#import "AppDelegate.h"

@interface Singleton : NSObject


@property(nonatomic,strong) NSString *baseUrl;
@property(nonatomic,strong) NSString *userName;

+ (Singleton *)sharedSingleton;
-(BOOL)is_iPhone5;
- (BOOL) validateEmail: (NSString *) checkString;

- (NSString *) getTrackListDirectoryPath;

@end
